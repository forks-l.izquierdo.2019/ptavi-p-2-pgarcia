"Ejercicio 8"

import unittest
from soundops import soundadd
from mysound import Sound


class TestSoundAdd(unittest.TestCase):
    def test_soundadd_equal_lengths(self):
        # Prueba la suma de dos sonidos con la misma longitud
        s1 = Sound(1.0)
        s1.sin(440, 10000)

        s2 = Sound(1.0)
        s2.sin(880, 5000)

        result_sound = soundadd(s1, s2)
        self.assertIsInstance(result_sound, Sound)
        self.assertEqual(result_sound.duration, 1.0)
        self.assertEqual(len(result_sound.buffer), len(s1.buffer))

    def test_soundadd_different_lengths(self):
        # Prueba la suma de dos sonidos con diferentes longitudes
        s1 = Sound(2.0)
        s1.sin(440, 10000)

        s2 = Sound(1.0)
        s2.sin(880, 5000)

        result_sound = soundadd(s1, s2)
        self.assertIsInstance(result_sound, Sound)
        self.assertEqual(result_sound.duration, 2.0)
        self.assertEqual(len(result_sound.buffer), len(s1.buffer))

    def test_soundadd_longer_s2(self):
        # Prueba la suma de dos sonidos donde s2 es más largo que s1
        s1 = Sound(1.0)
        s1.sin(440, 10000)

        s2 = Sound(2.0)
        s2.sin(880, 5000)

        result_sound = soundadd(s1, s2)
        self.assertIsInstance(result_sound, Sound)
        self.assertEqual(result_sound.duration, 2.0)
        self.assertEqual(len(result_sound.buffer), len(s2.buffer))
